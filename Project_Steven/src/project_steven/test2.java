
package project_steven;

/**
 *
 * @author marioalonso
 */
public class test2 {
    String surname;

    public test2() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "test2{" + "surname=" + surname + '}';
    }
    
    
    
}
