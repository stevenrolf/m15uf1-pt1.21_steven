package project_steven;

/**
 *
 * @author stevenrolf
 */
public class test {

    String name;
    String surname;

    public test() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "NewClass{" + "name=" + name + ", surname=" + surname + '}';
    }
    
    
    
}
