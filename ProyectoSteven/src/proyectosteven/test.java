/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectosteven;

import java.util.Objects;

/**
 *
 * @author jimmy
 */
public class test {
    public String home;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.home);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final test other = (test) obj;
        if (!Objects.equals(this.home, other.home)) {
            return false;
        }
        return true;
    }

    
    
}

